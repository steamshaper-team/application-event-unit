#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
# ${camel_case_unit-name} HTM Unit


## Resources Structure
```
│   .gitignore
│   pom.xml
│   README.md
│
└───src
    ├───main
    │   ├───extfs
    │   │   ├───conf
    │   │   │       readme.md
    │   │   │
    │   │   ├───dev
    │   │   │       readme.md
    │   │   │
    │   │   └───prod
    │   │           readme.md
    │   │
    │   ├───frontend
    │   │       ${unitname}-controller.js
    │   │       ${unitname}-service.js
    │   │       ${unitname}.js
    │   │       ${unitname}.less
    │   │       page-${unitname}-index.jade
    │   │       widget-${unitname}-directive.js
    │   │       widget-${unitname}.jade
    │   │
    │   ├───java
    │   │   ├───ioc
    │   │   │   └───app
    │   │   │       └───config
    │   │   │           └───${unitname}
    │   │   │                   ${camel_case_unit_name}Config.java
    │   │   │
    │   │   └───${unitname}
    │   │       ├───controllers
    │   │       │       ${camel_case_unit_name}Controller.java
    │   │       │
    │   │       ├───model
    │   │       │   │   I${camel_case_unit_name}Model.java
    │   │       │   │   ${camel_case_unit_name}Model.java
    │   │       │   │
    │   │       │   └───bean
    │   │       │           ${camel_case_unit_name}Bean.java
    │   │       │
    │   │       └───repos
    │   │           │   ${camel_case_unit_name}EntityRepo.java
    │   │           │
    │   │           └───jpa
    │   │               └───entities
    │   │                       ${camel_case_unit_name}Entity.java
    │   │
    │   └───resources
    │       └───META-INF
    │           └───spring
    │                   ${unitname}-ctx.xml
    │
    └───test
        ├───java
        │       readme.md
        │
        └───resources
                readme.md

```
### Requisiti
1. HTM Symlink [TBD](http://)




![To be continued](https://dl.dropboxusercontent.com/u/5384652/tbc.png)