package application_events.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;
import org.steamshaper.htm.HTM;
import org.steamshaper.htm.diagnostic.IProbe;
import org.steamshaper.htm.event.IHTMEventBus;
import org.steamshaper.htm.events.MonkeyEvent;
import org.steamshaper.htm.events.Tag;
import org.steamshaper.htm.events.selectors.IEventSelector;
import org.steamshaper.htm.remoting.ServiceRequest;
import org.steamshaper.htm.services.MonkeyRequest;
import org.steamshaper.htm.services.services.AbstractHTMService;
import org.steamshaper.htm.services.services.MonkeyServiceState;

import application_events.api.EventRaiseResult;
import application_events.api.IApplicationEvent;
import application_events.api.IApplicationEventBroadcaster;
import application_events.api.IApplicationEventsReceiver;
import application_events.model.internals.InterestIndex;
import application_events.model.internals.NewIntrestFoundException;

/**
 * @author danielefiungo
 *
 */
public class ApplicationEventBrodcaster extends AbstractHTMService
		implements IApplicationEventBroadcaster, ApplicationContextAware {

	public ApplicationEventBrodcaster() {
		super("steamshaper", "application_events", "1.0.0");
	}

	private boolean initialized = false;
	private Object initMonitor = new Object();
	private ApplicationContext applicationContext;

	/** The Constant log. */
	private static final IProbe log = HTM.diagnostic.getProbe(IApplicationEventBroadcaster.class.getName());

	private IApplicationEventsReceiver[] receivers;

	private InterestIndex idx_Intrest2Received = new InterestIndex();

	@Autowired
	private IHTMEventBus eventBus;

	@Override
	public EventRaiseResult txEvent(IApplicationEvent<?> event, PropagationMode mode) {
		EventRaiseResult output;
		if (initialized) {
			output = doTX(event, mode);
		} else {
			synchronized (initMonitor) {
				if (!initialized) { // Needed to avoid double initialization
									// caused by multiple raise event while
									// initializing

					scanReceiversInContext();

					this.initialized = true;
				}
			}
			output = doTX(event, mode);
		}

		return output;
	}

	private void scanReceiversInContext() {
		// ADD Service from spring context
		Map<String, IApplicationEventsReceiver> servicesCfg = applicationContext
				.getBeansOfType(IApplicationEventsReceiver.class);
		receivers = new IApplicationEventsReceiver[servicesCfg.size()];
		this.receivers = servicesCfg.values().toArray(receivers);
	}

	private EventRaiseResult doTX(IApplicationEvent<?> event, PropagationMode mode) {
		List<IApplicationEventsReceiver> receivers;
		try {
			receivers = whoIsIntrested(event);
		} catch (NewIntrestFoundException e) {
			// Means that this interest category is new than will be dispatched
			// to all known receivers
			receivers = Arrays.asList(this.receivers);
		}

		switch (mode) {
		case SYNC:
			dispatchSyncronized(event, receivers);
			break;
		case ASYNC:
			dispatchAsync(event, receivers);
			break;
		default:
			dispatchSyncronized(event, receivers); // To be replaced with ASYN
													// and PARALLEL MODE
			break;
		}

		EventRaiseResult eventRaiseResult = new EventRaiseResult();
		return eventRaiseResult;
	}

	private void dispatchAsync(IApplicationEvent<?> event, List<IApplicationEventsReceiver> receivers2) {

		MonkeyEvent monkeyEvent = new MonkeyEvent();
		Tag<IApplicationEvent<?>> eventTag = new Tag<IApplicationEvent<?>>(
				IApplicationEventBroadcaster.TAGS.APPLICAITON_EVENT, event);
		Tag<List<IApplicationEventsReceiver>> asyncEventTag = new Tag<List<IApplicationEventsReceiver>>(
				IApplicationEventBroadcaster.TAGS.ASYNC_APPLICAITON_EVENT, receivers2);

		monkeyEvent.addTag(eventTag);
		monkeyEvent.addTag(asyncEventTag);

		eventBus.fireEvent(ServiceRequest.getRequest(monkeyEvent));

	}

	private void dispatchSyncronized(IApplicationEvent<?> event, List<IApplicationEventsReceiver> receivers) {
		for (IApplicationEventsReceiver receiver : receivers) {
			try {
				if (receiver.receive(event)) {
					idx_Intrest2Received.addIntrest(event.getCategory(), receiver);
				}
			} catch (Exception ex) {
				// if a event received will throw an error the dispatching will
				// continue

				// TODO if an exception is thrown doesen't means that receiver
				// isn't interested some correction strategy should be
				// implemented
				log.error("Receiver %s on dispatch Exception %s\n%s", receiver.getClass().getSimpleName(),
						ex.getClass().getSimpleName(), ex.getMessage());

			}
		}
	}

	private List<IApplicationEventsReceiver> whoIsIntrested(IApplicationEvent<?> event)
			throws NewIntrestFoundException {

		return idx_Intrest2Received.search(event.getCategory());
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;

	}

	@Override
	public IEventSelector getEventSelector() {
		// TODO Auto-generated method stub
		return new IEventSelector() {

			@Override
			public boolean selectEvent(MonkeyEvent event) {
				return event.containsOneTagTypes(IApplicationEventBroadcaster.TAGS.APPLICAITON_EVENT,
						IApplicationEventBroadcaster.TAGS.ASYNC_APPLICAITON_EVENT);
			}

			@Override
			public boolean isEnabled() {
				return true;
			}
		};
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doExecute(MonkeyRequest<?> monkeyRequest) {
		if (monkeyRequest.getPayload() instanceof MonkeyEvent) {
			MonkeyEvent event = (MonkeyEvent) monkeyRequest.getPayload();
			Tag<List<IApplicationEventsReceiver>> receivers = (Tag<List<IApplicationEventsReceiver>>) event
					.getByType(IApplicationEventBroadcaster.TAGS.ASYNC_APPLICAITON_EVENT);
			Tag<IApplicationEvent<?>> appEvent = (Tag<IApplicationEvent<?>>) event
					.getByType(IApplicationEventBroadcaster.TAGS.APPLICAITON_EVENT);
			Assert.notNull(appEvent.getValue(), "");
			Assert.notNull(receivers.getValue(), "");

			dispatchSyncronized(appEvent.getValue(), receivers.getValue());
		}

	}

	@Override
	protected MonkeyServiceState doInit() {
		return MonkeyServiceState.INITIALIZED;
	}

	@Override
	protected MonkeyServiceState doStart() {
		return MonkeyServiceState.STARTED;
	}

	@Override
	protected MonkeyServiceState doStop() {
		return MonkeyServiceState.STOPPED;
	}

}
