package application_events.model.internals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import application_events.api.IApplicationEventsReceiver;

public class InterestIndex {

	Map<String,List<IApplicationEventsReceiver>> index = new ConcurrentHashMap<String,List<IApplicationEventsReceiver>>(10, 0.75f, 1);

	public void addIntrest(String interest, IApplicationEventsReceiver doc) {
		interest = normalizeInterest(interest);

		try {
			List<IApplicationEventsReceiver> actualIntrest = this.search(interest);
			if (actualIntrest.contains(doc)) {
				System.out.println("Intrest already expressed for " + doc + " on " + interest);
			} else {

				List<IApplicationEventsReceiver> idx = index.get(interest);
				addInterest2IdxTuple(idx,doc);
				System.out.println("added " + doc + " on " + interest);
			}
		} catch (NewIntrestFoundException e) {
			List<IApplicationEventsReceiver> idx = createInterestContained(interest);
			addInterest2IdxTuple(idx, doc);
			System.out.println("added " + doc + " on NEW " + interest);
		}
	}

	private void addInterest2IdxTuple(List<IApplicationEventsReceiver> idx, IApplicationEventsReceiver doc) {
		synchronized (idx) {
			idx.add(doc);
		}
		
	}

	private List<IApplicationEventsReceiver> createInterestContained(String intrest) {
		List<IApplicationEventsReceiver> idx = new ArrayList<IApplicationEventsReceiver>();
		index.put(intrest, idx);
		return idx;
	}


	public List<IApplicationEventsReceiver> search(String interest) throws NewIntrestFoundException {
		interest = normalizeInterest(interest);

		List<IApplicationEventsReceiver> interested = index.get(interest);

		if (interested != null) {

			int resultsCount = interested.size();
			List<IApplicationEventsReceiver> output = new ArrayList<IApplicationEventsReceiver>(resultsCount);

			IApplicationEventsReceiver[] docs = new IApplicationEventsReceiver[resultsCount];
			docs = interested.toArray(docs);

			for (IApplicationEventsReceiver doc : docs) {
				output.add(doc);
			}
			System.out.print("Intrest [" + interest + "] match (" + resultsCount + ")");
			return output;
		} else {
			createInterestContained(interest);
			throw new NewIntrestFoundException();
		}
	}

	private String normalizeInterest(String intrest) {
		return intrest.trim().toLowerCase();
	}

}
