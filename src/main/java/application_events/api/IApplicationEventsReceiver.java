package application_events.api;

public interface IApplicationEventsReceiver {
	
	
	boolean receive(IApplicationEvent<?> event);
	
	

}
