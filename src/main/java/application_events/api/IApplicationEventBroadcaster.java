package application_events.api;

public interface IApplicationEventBroadcaster {

	public enum PropagationMode{
		SYNC, ASYNC, PARALLEL
	}

	public final static Tags  TAGS = new Tags();
	
	public class Tags{
		private Tags(){
			
		}
		
		
		public final String ASYNC_APPLICAITON_EVENT = "ASYNC_APPLICATION_EVENT_TAG";
		public final String APPLICAITON_EVENT = "APPLICATION_EVENT_TAG";
		
		
	}

	EventRaiseResult txEvent(IApplicationEvent<?> event, PropagationMode mode);

}
