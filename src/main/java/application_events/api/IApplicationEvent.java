package application_events.api;

import java.util.Map;

/**
 * The Interface IApplicationEvent.
 *
 * @param <T> the generic type
 */
public interface IApplicationEvent<T> {
	
	
	/**
	 * Gets the event category.
	 *
	 * @return the category
	 */
	public String getCategory();
	
	
	/**
	 * Gets the Subject OF Analysis.
	 *
	 * @return the sofa
	 */
	public Map<String, Object> getSofa();
	
	/**
	 * Add a property to the SOFA.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public void addProperty(String name,Object value);
	
	
	
	
	

}
