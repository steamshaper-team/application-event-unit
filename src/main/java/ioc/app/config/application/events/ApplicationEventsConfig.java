package ioc.app.config.application.events;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/META-INF/spring/application_events-ctx.xml")
public class ApplicationEventsConfig {

}
